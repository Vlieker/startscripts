shift 2
for arg in "$@"
do
	case $arg in
	-name)
	server_name=$2
	shift 2
	;;
	-password)
	server_password=$2
	shift 2
	;;
	-port)
	server_port=$2
	shift 2
	;;
	-world)
	server_world=$2
	shift 2
	;;
	-public)
	server_public=$2
	shift 2
	;;
	-savedir)
	server_savedir=$2
	shift 2
	;;
	-logfile)
	server_logfile=$2
	shift 2
	;;
	-vhplus)
	server_vhplus=$2
	shift 2
	;;
	esac
done
if [ "$server_vhplus" -eq "1" ]; then
	"./vhstartscript_bepinex.sh" -name "${server_name}" -password "${server_password}" -port "${server_port}" -world "${server_world}" -public "${server_public}" -savedir "${server_savedir}" -logfile "${server_logfile}" -nographics -batchmode
else
	export templdpath=$LD_LIBRARY_PATH
	export LD_LIBRARY_PATH=./linux64:$LD_LIBRARY_PATH
	export SteamAppId=892970
	"./valheim_server.x86_64" -nographics -batchmode -name "${server_name}" -password "${server_password}" -port "${server_port}" -world "${server_world}" -public "${server_public}" -savedir "${server_savedir}" -logfile "${server_logfile}" -console
	export LD_LIBRARY_PATH=$templdpath
fi
